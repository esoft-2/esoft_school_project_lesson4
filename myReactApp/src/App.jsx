import './App.css'
import React from 'react'
import { Timer } from './Commponents/Timer'
import { UserList } from './Commponents/UserList'
import { WindowSize } from './Commponents/WindowSize'

function App() {


  return (
    <>
      <Timer />
      <UserList />
      <WindowSize />
    </>
  )
}

export default App
