import { useEffect, useState } from "react"


export const UserList = () => {
    const [data, setData] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            try{
                const response = await fetch('https://jsonplaceholder.typicode.com/users') 
                if (!response) {
                    return null
                }
                const newdata = await response.json()
                setData(newdata)
            } catch (error) {
                console.error(error)
            }
        }

        fetchData()
    }, [])

    return(
        <>
            <h1>USER LIST</h1>
            <div>
                <ul>
                    {data.map(user => (
                        <li key={user.id}>{user.name}</li>
                    ))}
                </ul>
            </div>
        </>
    )
}