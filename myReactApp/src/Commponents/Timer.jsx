import { useEffect, useState } from "react"
import React from "react"

export const Timer = () => {
    const [timer, setTimer] = useState(10)

    useEffect(() => {
        const intervalId = setInterval(() => {
            if (timer > 0) {
                setTimer(prev => prev - 1)
            }
        }, 1000)

        return () => clearInterval(intervalId)
    }, [timer])

    return(
        <div>
            Timer: {timer} seconds
        </div>
    )
}

